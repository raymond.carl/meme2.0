//
//  DataController.swift
//  Meme 1.0
//
//  Created by Raymond Carl on 15/06/2020.
//  Copyright © 2020 Raymond Carl. All rights reserved.
//

import Foundation
import CoreData

var memes: [Meme] = []

class DataController {
    
    var viewContext:NSManagedObjectContext {
        return persistentContainer.viewContext
    }
    
    let persistentContainer:NSPersistentContainer
    
    init (modelName:String){
        persistentContainer = NSPersistentContainer(name: modelName)
    }
    
    func load(completion: (() -> Void)? = nil){
        persistentContainer.loadPersistentStores { storeDescription, error in
        
            guard error == nil else {
                fatalError(error!.localizedDescription)
            }
            completion?()
        }
    }
    
}
