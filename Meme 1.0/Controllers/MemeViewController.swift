//
//  MemeViewController.swift
//  Meme 1.0
//
//  Created by Raymond Carl on 21/06/2020.
//  Copyright © 2020 Raymond Carl. All rights reserved.
//

import Foundation
import UIKit

class MemeViewController: UIViewController {
    
    @IBOutlet weak var memedImage: UIImageView!
    
    var meme: Meme?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        // Handle the text field’s user input through delegate callbacks.
        //nameTextField.delegate = self

        // Set up views if editing an existing Meal.
        if let meme = meme {
            //let image = UIImage(data: meme.memedImage!)
            memedImage.image = UIImage(data: meme.memedImage!)
                
                //UIImage(data: meme.memedImage ?? Data())
        }
    }

    
}
