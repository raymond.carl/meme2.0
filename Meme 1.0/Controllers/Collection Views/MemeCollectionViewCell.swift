//
//  MemeCollectionViewCell.swift
//  Meme 1.0
//
//  Created by Raymond Carl on 27/06/2020.
//  Copyright © 2020 Raymond Carl. All rights reserved.
//

import UIKit

class MemeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var memeImageView: UIImageView!
    
}
