//
//  MemeCollectionViewController.swift
//  Meme 1.0
//
//  Created by Raymond Carl on 27/06/2020.
//  Copyright © 2020 Raymond Carl. All rights reserved.
//

import UIKit
import CoreData
import os.log

private let reuseIdentifier = "Cell"

class MemeCollectionViewController: UICollectionViewController {

    @IBOutlet weak var flowLayout: UICollectionViewFlowLayout!
    
    var dataController:DataController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let space:CGFloat = 3.0
        let dimension = (view.frame.size.width - (2 * space)) / 3.0

        flowLayout.minimumInteritemSpacing = space
        flowLayout.minimumLineSpacing = space
        flowLayout.itemSize = CGSize(width: dimension, height: dimension)
        
        

        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        dataController = appDelegate.dataController
                
        let fetchRequest:NSFetchRequest<Meme> = Meme.fetchRequest()
        
        let sortDescriptor = NSSortDescriptor(key: "bottomText", ascending: false)
        fetchRequest.sortDescriptors = [sortDescriptor]

        if let result = try?
            dataController.viewContext.fetch(fetchRequest) {
            memes = result
            
            DispatchQueue.main.async {
             self.collectionView.reloadData()
            }
        }
        

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
        self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)

        // Do any additional setup after loading the view.
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
     

            // Get the new view controller using segue.destination.
            // Pass the selected object to the new view controller.
            
            super.prepare(for: segue, sender: sender)
            
            switch(segue.identifier ?? "") {
                
            case "NewMeme":
                os_log("Adding a new meme.", log: OSLog.default, type: .debug)
            
            
            case "MemeDetail":
                guard let memeViewController = segue.destination as? MemeViewController
                    else {
                        fatalError("Unexpected destination: \(segue.destination)")
                    }
                guard let selectedMemeCell = sender as? MemeCollectionViewCell
                    else {
                        fatalError("Unexpected sender: \(sender)")
                    }
                guard let indexPath = self.collectionView.indexPath(for: selectedMemeCell) else {
                    fatalError("The selected cell is not being displayed by the table")
                }
                
                let selectedMeme = memes[indexPath.row]
                
                memeViewController.meme = selectedMeme
                
                //memeDetailViewController.memedImage?.image = UIImage(data: selectedMeme.memedImage ?? Data())
            
            default:
                fatalError("Unexpected Segue Identifier; \(segue.identifier)")
                
            }
    
    }
    

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return memes.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        // Configure the cell
        let cellIdentifier = "MemeCollectionViewCell"
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as? MemeCollectionViewCell  else {
                   fatalError("The dequeued cell is not an instance of MemeCollectionViewCell")
               }
        /*
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? MemeTableViewCell else {
            fatalError("The dequeued cell is not an instance of MemeTableViewCell")
        }
        */
        
        let meme = memes[indexPath.row]
        
        cell.memeImageView?.image = UIImage(data: meme.memedImage ?? Data())
        //cell.nameLabel.text = "placeholder"
        
        return cell
    }

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}
