//
//  MemeTableViewCell.swift
//  Meme 1.0
//
//  Created by Raymond Carl on 15/06/2020.
//  Copyright © 2020 Raymond Carl. All rights reserved.
//

import UIKit

class MemeTableViewCell: UITableViewCell {

    
    
    //MARK: Properties
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var photoImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
