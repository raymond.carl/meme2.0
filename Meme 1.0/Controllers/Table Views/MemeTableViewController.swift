//
//  MemeTableViewController.swift
//  Meme 1.0
//
//  Created by Raymond Carl on 15/06/2020.
//  Copyright © 2020 Raymond Carl. All rights reserved.
//

import UIKit
import CoreData
import os.log

class MemeTableViewController: UITableViewController {

    var dataController:DataController!

    override func viewDidLoad() {
        super.viewDidLoad()

        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        dataController = appDelegate.dataController
                
        let fetchRequest:NSFetchRequest<Meme> = Meme.fetchRequest()
        
        let sortDescriptor = NSSortDescriptor(key: "bottomText", ascending: false)
        fetchRequest.sortDescriptors = [sortDescriptor]

        if let result = try?
            dataController.viewContext.fetch(fetchRequest) {
            memes = result
            tableView.reloadData()
        }
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return memes.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "MemeTableViewCell"

        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? MemeTableViewCell else {
            fatalError("The dequeued cell is not an instance of MemeTableViewCell")
        }
        
        let meme = memes[indexPath.row]
        
        cell.imageView?.image = UIImage(data: meme.memedImage ?? Data())//meme.memedImage
        cell.nameLabel.text = "placeholder"
        
        return cell
    }
    
    
    // MARK: Delete Meme
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let meme = memes[indexPath.row]
            dataController.viewContext.delete(meme)
            memes.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)

            try? dataController.viewContext.save()
        }
    }

    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        super.prepare(for: segue, sender: sender)

        
        switch(segue.identifier ?? "") {
        
        case "NewMeme":
            os_log("Adding a new meme.", log: OSLog.default, type: .debug)
        
        
        case "MemeDetail":
            guard let memeViewController = segue.destination as? MemeViewController
                else {
                    fatalError("Unexpected destination: \(segue.destination)")
                }
            guard let selectedMemeCell = sender as? MemeTableViewCell
                else {
                    fatalError("Unexpected sender: \(sender)")
                }
            guard let indexPath = tableView.indexPath(for: selectedMemeCell) else {
                fatalError("The selected cell is not being displayed by the table")
            }
            
            let selectedMeme = memes[indexPath.row]
            memeViewController.meme = selectedMeme
            
            //memeDetailViewController.memedImage?.image = UIImage(data: selectedMeme.memedImage ?? Data())
        
        default:
            fatalError("Unexpected Segue Identifier; \(segue.identifier)")
            
        }
        
    }
    

}
