//
//  textDelegate.swift
//  test
//
//  Created by Raymond Carl on 04/04/2020.
//  Copyright © 2020 Raymond Carl. All rights reserved.
//

import Foundation
import UIKit


class textDelegate: NSObject, UITextFieldDelegate {

    let memeTextAttributes: [NSAttributedString.Key: Any] = [
        NSAttributedString.Key.strokeColor: UIColor.black,
        NSAttributedString.Key.foregroundColor: UIColor.white,
        NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-CondensedBlack", size: 40)!,
        NSAttributedString.Key.strokeWidth:  -4.0
    ]
    

    override init() {
        super.init()

    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        textField.textColor = UIColor.white
        textField.tintColor = UIColor.white
        textField.defaultTextAttributes = memeTextAttributes
        textField.textAlignment = .center
    
        return true
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = ""
    }
    
}
